<?php

class Bootstrap {
    
    private $controllers_directory = 'controllers';
    private $models_directory = 'models';
    private $libs_directory = 'libs';
    
	function __construct() {
        
        session_start();
        
        $this->loadLibs();
        $this->loadModels();
        
		$url = isset($_GET['url']) ? $_GET['url'] : null;
		$url = rtrim($url, '/');
		$url = explode('/', $url);
        
		if (empty($url[0])) {
			require_once $this->controllers_directory.'/index.php';
			$controller = new Index();
			$controller->index();
			return false;
		}

		$file = $this->controllers_directory.'/' . $url[0] . '.php';
		if (file_exists($file)) {
			require_once $file;
		}
        else {
			$this->error();
            return false;
		}
		
		$controller = new $url[0];

        if (isset($url[1])) {
            if (method_exists($controller, $url[1])) {
                $controller->{$url[1]}();
            }
            else {
                $this->error();
            }
        }
        else {
            $controller->index();
        }
        
	}
	
	private function error() {
		require_once $this->controllers_directory.'/error.php';
		$controller = new error();
		$controller->index();
		return false;
	}
    
    private function loadLibs()
    {
        if ($libs_directory = opendir($this->libs_directory)) {
            while ( false !== ($file = readdir($libs_directory))) {
                if ($file!='.' && $file!='..') {
                    require_once $this->libs_directory.'/'.$file;
                }
            }
            closedir($libs_directory);
        }
    }
    
    private function loadModels()
    {
        if ($models_directory = opendir($this->models_directory)) {
            while ( false !== ($file = readdir($models_directory))) {
                if ($file!='.' && $file!='..') {
                    require_once $this->models_directory.'/'.$file;
                }
            }
            closedir($models_directory);
        }
    }
    
}