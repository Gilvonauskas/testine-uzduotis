<?php

class View {

	public function __construct() {
		
	}

	public function render($name, $noInclude = false)
	{
		if ($noInclude == true) {
			require 'views/' . $name . '.php';	
		}
		else {
            $this->cart_total_quantity = $this->countCartItemsTotal();
			require_once 'views/header.php';
			require_once 'views/' . $name . '.php';
			require_once 'views/footer.php';	
		}
	}
    
    private function countCartItemsTotal()
    {
        if (isset($_COOKIE['cart_items_cookie'])) {
            $cart_items = json_decode($_COOKIE['cart_items_cookie'], true);
            $total = 0;
            foreach ($cart_items as $cart_item) {
                $total = $total + $cart_item['quantity'];
            }
            return $total;
        }
        else {
            return 0;
        }
    }
    
}