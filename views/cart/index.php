<div class="cart-wrapper">
    <?php if(count($this->products)>0): ?>
        <table class="table cart-table">
            <thead>
                <tr>
                    <th>Prekė</th>
                    <th class="text-right">Kaina</th>
                    <th class="text-center">Kiekis</th>
                    <th class="text-right">Viso</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <?php foreach($this->products as $product): ?>
                <tr class="item-row cart-product-row" id="product-row<?php echo $product['id']; ?>">
                    <td>
                        <img src="<?php echo BASE_URL . $product['img'] ?>" alt="<?php echo $product['title'] ?>" class="cart-img" />
                        <div class="cart-item-title text-center"><?php echo $product['title']; ?></div>
                    </td>
                    <td class="text-right">
                        <span class="product-unit-price-in-cart" data-id="<?php echo $product['id']; ?>">
                            <?php echo $product['price']; ?>
                        </span> &euro;
                    </td>
                    <td class="col-xs-1 text-center">
                        <input type="number" data-id="<?php echo $product['id']; ?>" value="<?php echo $this->quantities[$product['id']]['quantity']; ?>" class="form-control cart-quantity-input" min="1" max="<?php echo $product['amount']; ?>" />
                    </td>
                    <td class="text-right">
                        <span class="item-total-in-cart" data-id="<?php echo $product['id']; ?>">
                            <?php echo $this->quantities[$product['id']]['quantity'] * $product['price']; ?>
                        </span> &euro;
                    </td>
                    <td class="col-lg-1 text-center">
                        <button class="btn btn-danger btn-xs remove-item-from-cart" data-id="<?php echo $product['id']; ?>">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td><b>Viso:</b></td>
                <td colspan="3" class="text-right">
                    <b>
                        <span class="cart-total"><?php echo $this->total; ?></span>
                        &euro;
                    </b>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    <?php else: ?>
        <p>Šiuo metu jūsų krepšelyje produktų nėra.</p>
    <?php endif; ?>
</div>
<div class="checkout-form-wrapper">
    <?php if (isset($this->user_logged_in) && $this->user_logged_in): ?>
        <form method="post" action="<?php echo BASE_URL; ?>/order">
            <div class="pull-right">
            <input type="submit" value="Patvirtinti užsakymą" class="btn btn-primary" />
        </div>
        <div class="clear"></div>
        </form>
    <?php else: 
            $submit_btn_txt = 'Patvirtinti užsakymą';
            require_once 'views/order/index.php';
        endif;
    ?>
</div>