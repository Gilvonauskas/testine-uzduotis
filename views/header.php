<!doctype html>
<html>
<head>
	<title>Super Duper E-shop</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script type="text/javascript" src="<?php echo BASE_URL; ?>/public/libs/jquery-3.2.1.min.js"></script>
    <link href="<?php echo BASE_URL; ?>/public/libs/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <script src="<?php echo BASE_URL; ?>/public/libs/bootstrap-3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>/public/js/eshop-functions.js"></script>
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>/public/css/eshop.css" />
    <script src="<?php echo BASE_URL; ?>/public/libs/jquery-ui-1.12.1/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        baseUrl = '<?php echo BASE_URL; ?>';
    </script>
</head>
<body>
	
<div class="container main-wrapper">
    <div class="header">
        <a href="<?php echo BASE_URL; ?>" class="eshop-logo">
            <img src="<?php echo BASE_URL; ?>/public/img/eshop_logo.png" alt="Super E-shop Logo" />
        </a>
        <div class="top-right-btns-holder">
            <a href="<?php echo BASE_URL; ?>/order/orderslist" class="btn btn-primary pull-right">Peržiūrėti užsakymus</a>
            <a href="<?php echo BASE_URL; ?>/cart" class="btn btn-primary cart-indicator pull-right">
                <span class="glyphicon glyphicon-shopping-cart"></span>
                <span class="cart-total-quantity">
                    <?php echo $this->cart_total_quantity; ?>
                </span>
            </a>
        </div>
        <div class="clear"></div>
    </div>
	
	