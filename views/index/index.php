<div class="row">
    <?php if (count($this->products)>0): ?>
        <?php foreach ($this->products as $product): ?>
            <div class="width20proc">
                <div class="item-wrapper">
                    <img src="<?php echo BASE_URL . $product['img']; ?>" alt="<?php echo $product['title']; ?>" id="product_img<?php echo $product['id']; ?>" />
                    <div class="product-box-component"><?php echo $product['title']; ?></div>
                    <div class="product-box-component">Kaina: <?php echo $product['price']; ?> &euro;</div>
                    <?php if ($product['amount']>0): ?>
                        <div class="product-box-component"><button class="btn btn-primary add-to-cart" data-id="<?php echo $product['id'] ?>">Į krepšelį</button></div>
                    <?php else: ?>
                        <div class="product-box-component"><button class="btn">Prekės nėra sandėlyje</button></div>
                    <?php endif; ?>
                    <div class="clear"></div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <p>Prekių šiuo metu nėra</p>
    <?php endif; ?>
</div>
<?php if (count($this->products)>0): ?>
    <hr/>
    <div class="pull-right">
        <a href="<?php echo BASE_URL; ?>/cart" class="btn btn-primary">Užsakyti</a>
    </div>
    <div class="clear"></div>
<?php endif; ?>
