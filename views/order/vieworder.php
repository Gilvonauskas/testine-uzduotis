<p><b>Užsakymo data:</b> <?php echo $this->order['date']; ?></p>
<p><b>Užsakymo būsena:</b>
    <?php if($this->order['state'] == 1): ?>
        <span class="label label-success">Aktyvus</span>
    <?php else: ?>
        <span class="label label-danger">Atšauktas</span>
    <?php endif; ?>
</p>
<table class="table cart-table">
    <thead>
        <tr>
            <th>Paveikslėlis</th>
            <th class="text-right">Kaina</th>
            <th class="text-center">Kiekis</th>
            <th class="text-right">Viso</th>
        </tr>
    </thead>
    <?php foreach($this->order['items'] as $product): ?>
        <tr class="item-row cart-product-row">
            <td>
                <img src="<?php echo BASE_URL . $product['product']['img']; ?>" alt="<?php echo $product['product']['title'] ?>" class="cart-img" />
                <div class="cart-item-title text-center"><?php echo $product['product']['title']; ?></div>
            </td>
            <td class="text-right">
                <span class="product-unit-price-in-cart">
                    <?php echo $product['product']['price']; ?>
                </span> &euro;
            </td>
            <td class="text-center">
                <?php echo $product['quantity']; ?>
            </td>
            <td class="text-right">
                <span class="item-total-in-cart">
                    <?php echo $product['product_total_price']; ?>
                </span> &euro;
            </td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td><b>Viso:</b></td>
        <td colspan="3" class="text-right">
            <b>
                <span class="cart-total"><?php echo $this->order['cart_total_price']; ?></span>
                &euro;
            </b>
        </td>
    </tr>
</table>
<?php if($this->order['state'] == 1): ?>
    <form method="post" action="<?php echo BASE_URL; ?>/order/cancelorder">
        <input type="hidden" name="id" value="<?php echo $this->order_id; ?>" />
        <div class="pull-right">
            <a href="<?php echo BASE_URL; ?>/order/orderslist" class="btn btn-success">
                <span class="glyphicon glyphicon-chevron-left"></span>
                Atgal
            </a>
            <button type="submit" class="btn btn-danger">
                <span class="glyphicon glyphicon-remove"></span>
                Atšaukti užsakymą
            </button>
        </div>
        <div class="clear"></div>
    </form>
<?php else: ?>
    <div class="pull-right">
        <a href="<?php echo BASE_URL; ?>/order/orderslist" class="btn btn-success">
            <span class="glyphicon glyphicon-chevron-left"></span>
            Atgal
        </a>
    </div>
    <div class="clear"></div>
<?php endif; ?>