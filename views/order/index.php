<?php if(isset($this->errors) && count($this->errors)>0): ?>
    <ul class="alert alert-danger">
        <?php foreach($this->errors as $error): ?>
            <li><?php echo $error; ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
<form method="post" action="<?php echo BASE_URL; ?>/order">
    <div class="form-group">
        <label for="inputName">Vardas: <span class="red">*</span></label>
        <input type="text" name="name" class="form-control" id="inputName" placeholder="Vardas" value="<?php if(isset($this->field_values['name'])) echo $this->field_values['name']; ?>" />
    </div>
    <div class="form-group">
        <label for="inputSurname">Pavardė: <span class="red">*</span></label>
        <input type="text" name="surname" class="form-control" id="inputSurname" placeholder="Pavardė" value="<?php if(isset($this->field_values['surname'])) echo $this->field_values['surname']; ?>" />
    </div>
    <div class="form-group">
        <label for="inputEmail">El. paštas: <span class="red">*</span></label>
        <input type="email" name="email" class="form-control" id="inputEmail" placeholder="El. paštas" value="<?php if(isset($this->field_values['email'])) echo $this->field_values['email']; ?>" />
    </div>
    <div class="pull-right">
        <?php
            if (!isset($submit_btn_txt)) {
                $submit_btn_txt = 'Prisijungti';
            }
        ?>
        <input type="submit" value="<?php echo $submit_btn_txt; ?>" class="btn btn-primary" />
    </div>
    <div class="clear"></div>
    <div class="">
        <span class="red">*</span> - taip pažymėti laukai yra privalomi
    </div>
</form>