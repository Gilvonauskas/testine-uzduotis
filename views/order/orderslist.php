<div class="">
    <?php if(isset($this->messages) && count($this->messages)>0): ?>
        <div class="alert alert-info">
            <?php foreach($this->messages as $message): ?>
                <p><?php echo $message; ?></p>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($this->orders) && count($this->orders)): ?>
        <table class="table">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Būsena</th>
                    <th></th>
                </tr>
            </thead>
            <?php foreach($this->orders as $key=>$order): ?>
                <tr>
                    <td><?php echo $order['date']; ?></td>
                    <td>
                        <?php if($order['state'] == 1): ?>
                            <span class="label label-success">Aktyvus</span>
                        <?php else: ?>
                            <span class="label label-danger">Atšauktas</span>
                        <?php endif; ?>
                    </td>
                    <td class="text-right">
                        <a href="<?php echo BASE_URL ?>/order/vieworder?id=<?php echo $key; ?>" class="btn btn-primary btn-xs">Žiūrėti</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php else: ?>
        <p>Šiuo metu užsakymų neturite</p>
    <?php endif; ?>
</div>