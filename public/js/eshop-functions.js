/* 
 * License: free to use
 */

$(document).ready(function(){
    
    $('.add-to-cart').click(function(){
        var id = $(this).attr('data-id');
        $.ajax({
            url: baseUrl + "/cart/addtocart",
            data: 'product_id='+id,
            success: function(){
                recalcShoppingCartIconTotal();
                addToCartEffects(id);
            }
        });
    });
    
    $('.cart-quantity-input').change(function(){
        $.ajax({
            url: baseUrl + '/cart/changequantity',
            data: {
                product_id: $(this).attr('data-id'),
                quantity: $(this).val()
            },
            success: function(){
                recalculateCart();
            }
        });
    });
    
    $('.remove-item-from-cart').click(function(){
        var objectToRemove = $(this).parent().parent();
        $.ajax({
            url: baseUrl + '/cart/changequantity',
            data: {
                product_id: $(this).attr('data-id'),
                quantity: 0
            },
            success: function(){
                objectToRemove.remove();
                if ($('.cart-product-row').length == 0) {
                    $('.cart-table').remove();
                    $('.cart-wrapper').text('Jūsų krepšelyje nebėra prekių');
                }
                recalculateCart();
            }
        });
    });
    
});

function recalcShoppingCartIconTotal() {
    var total = Number($('.cart-total-quantity').text());
    total++;
    $('.cart-total-quantity').text(total);
}

function addToCartEffects(id) {
    var cart = $('.cart-indicator');
    var imgtodrag = $('#product_img'+id);
    if (imgtodrag) {
        var imgclone = imgtodrag.clone()
            .offset({
            top: imgtodrag.offset().top,
            left: imgtodrag.offset().left
        })
            .css({
            'opacity': '0.5',
                'position': 'absolute',
                'height': '150px',
                'width': '150px',
                'z-index': '100'
        })
            .appendTo($('body'))
            .animate({
            'top': cart.offset().top + 10,
                'left': cart.offset().left + 10,
                'width': 75,
                'height': 75
        }, 1000, 'easeInOutExpo');
        
        setTimeout(function () {
            cart.effect("shake", {
                times: 2
            }, 200);
        }, 1500);
        
        imgclone.animate({
            'width': 0,
                'height': 0
        }, function () {
            $(this).detach();
        });
    }
    
}

function recalculateCart() {
    var rowTotalPrice = 0;
    var unitPrice = 0;
    var itemTotal = 0;
    var priceTotal = 0;
    var totalSumOfItems = 0;
    $('.cart-product-row').each(function(){
        unitPrice = Number($(this).find('.product-unit-price-in-cart').text());
        itemTotal = Number($(this).find('.cart-quantity-input').val());
        totalSumOfItems = totalSumOfItems + itemTotal;
        rowTotalPrice = Number(unitPrice * itemTotal);
        $(this).find('.item-total-in-cart').text(rowTotalPrice.toFixed(2));
        priceTotal = priceTotal + rowTotalPrice;
    });
    $('.cart-total-quantity').text(totalSumOfItems);
    $('.cart-total').text(priceTotal.toFixed(2));
}
