<?php

/*
 * License: free to use.
 */

/**
 * 
 *
 * @author Tomas Gilvonauskas
 */
class Products {
    
    private static $file = 'data/products.txt';
    private static $imgs_directory = 'public/products_imgs';
    
    public static function seed($limit = 5)
    {
        $images = self::getRandomizedImagesList($limit);
        $products = array();
        $i = 1;
        for ($i=1; $i<=$limit; $i++) {
            $product['id'] = $i;
            $product['title'] = 'Prekė '.$i;
            $product['price'] = rand(1, 100).'.'.str_pad(rand(0,99), 2, '0', STR_PAD_LEFT);
            $product['amount'] = rand(1, 99);
            $product['img'] = $images[$i-1];
            $products[$i] = $product;
        }
        file_put_contents(self::$file, json_encode($products));
        return $products;
    }
    
    private static function getRandomizedImagesList($limit = 5)
    {
        $files = array();
        $i = 1;
        if ($imgs_directory = opendir(self::$imgs_directory)) {
            while ( false !== ($file = readdir($imgs_directory)) && $i<=$limit) {
                if ($file!='.' && $file!='..') {
                    $files[] = '/'.self::$imgs_directory.'/'.$file;
                    $i++;
                }
            }
            closedir($imgs_directory);
        }
        shuffle($files);
        return $files;
    }
    
    public static function getAllProducts()
    {
        return json_decode(file_get_contents(self::$file), true);
    }
    
    public static function getProduct($id)
    {
        $products = self::getAllProducts();
        foreach ($products as $product) {
            if ($product['id']==$id) {
                return $product;
            }
        }
        return false;
    }
    
    public static function getProductsByIds($ids = array())
    {
        $productsToReturn = array();
        $products = self::getAllProducts();
        foreach ($products as $id=>$product) {
            if (isset($product['id']) && in_array($product['id'], $ids)) {
                $productsToReturn[$id] = $product;
            }
        }
        return $productsToReturn;
    }
    
    public static function getProductsTotalPrice($products = array(), $itemsInCart = array())
    {
        $totalPrice = 0;
        foreach ($products as $product) {
            if (array_key_exists($product['id'], $itemsInCart)) {
                $totalPrice = $totalPrice + ($product['price'] * $itemsInCart[$product['id']]['quantity']);
            }
        }
        return number_format($totalPrice, 2);
    }
    
    public static function updateProductsAmountsAfterOrder($itemsInCart)
    {
        $products = self::getAllProducts();
        foreach ($itemsInCart as $id=>$item) {
            $products[$id]['amount'] = $products[$id]['amount'] - $item['quantity'];
        }
        file_put_contents(self::$file, json_encode($products));
    }
    
    public static function updateAmountsAfterCanceledOrder($order)
    {
        $products = self::getAllProducts();
        foreach($order['items'] as $item) {
            $products[$item['product']['id']]['amount'] = 
                    $products[$item['product']['id']]['amount'] + $item['quantity'];
        }
        file_put_contents(self::$file, json_encode($products));
    }
    
}
