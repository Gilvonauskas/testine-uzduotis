<?php

/*
 * License: free to use.
 */

/**
 * 
 *
 * @author Tomas Gilvonauskas
 */
class CartCookie {
    
    private static $cartCookieName = 'cart_items_cookie';
    private static $cartCookiePath = CART_COOKIE_PATH;
    private static $cookieLifetime = 2592000; // 30 days (in seconds)
    
    public static function getCartItems()
    {
        $cookie = isset($_COOKIE[self::$cartCookieName]) ? $_COOKIE[self::$cartCookieName] : '';
        $cookie = stripslashes($cookie);
        return json_decode($cookie, true);
    }
    
    public static function setCartItems($itemsInCart)
    {
        setcookie(self::$cartCookieName, "", time()-3600);
        $json = json_encode($itemsInCart, true);
        setcookie(self::$cartCookieName, $json, time() + (self::$cookieLifetime), self::$cartCookiePath);
        $_COOKIE[self::$cartCookieName]=$json;
    }
    
    public static function unsetCartCookie()
    {
        setcookie(self::$cartCookieName, "", time()-3600);
        unset($_COOKIE[self::$cartCookieName]);
    }
    
}
