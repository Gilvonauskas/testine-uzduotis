<?php

/*
 * License: free to use.
 */

/**
 * 
 *
 * @author Tomas Gilvonauskas
 */
class Orders {
    
    private static $file = 'data/orders.txt';
    
    public static function getAllOrders()
    {
        return json_decode(file_get_contents(self::$file), true);
    }
    
    public static function getAllOrdersByUserId($userId)
    {
        $orders = json_decode(file_get_contents(self::$file), true);
        if (isset($orders[$userId])) {
            return array_reverse($orders[$userId], true);
        }
    }
    
    public static function putNewOrder($itemsInCart, $productsInfo, $userId)
    {
        $orders = json_decode(file_get_contents(self::$file), true);
        if (isset($orders[$userId])) {
            $nextOrderId = count($orders[$userId]) + 1;
        }
        else {
            $nextOrderId = 1;
        }
        $cartTotalPrice = 0;
        foreach ($itemsInCart as $id=>$item) {
            $productTotalPrice = number_format($item['quantity'] * $productsInfo[$id]['price'], 2, '.', '');
            $items[] = array(
                'quantity'=>$item['quantity'],
                'product'=>$productsInfo[$id],
                'product_total_price'=> $productTotalPrice
            );
            $cartTotalPrice = $cartTotalPrice + $productTotalPrice;
        }
        $orders[$userId][$nextOrderId] = array(
            'items'=> $items,
            'date'=>date('Y-m-d H:i:s'),
            'state'=>1,
            'cart_total_price'=>number_format($cartTotalPrice, 2, '.', '')
        );
        file_put_contents(self::$file, json_encode($orders));
    }
    
    public static function getIndividualOrder($userId, $orderId)
    {
        $orders = json_decode(file_get_contents(self::$file), true);
        return $orders[$userId][$orderId];
    }
    
    public static function cancelOrder($userId, $orderId)
    {
        $orders = json_decode(file_get_contents(self::$file), true);
        $orders[$userId][$orderId]['state'] = 0;
        file_put_contents(self::$file, json_encode($orders));
        return $orders[$userId][$orderId];
    }
    
}
