<?php

/*
 * License: free to use.
 */

/**
 * 
 *
 * @author Tomas Gilvonauskas
 */
class LoginForm {
    
    public static function validate($postParams)
    {
        $fieldsToValidate = array('name', 'surname', 'email');
        $result['success'] = true;
        foreach ($fieldsToValidate as $field) {
            if (!isset($postParams[$field]) || empty($postParams[$field])) {
                $result['errors'][0] = 'Formoje yra neužpildytų privalomų laukų';
                $result['success'] = false;
            }
            else {
                $result['field_values'][$field] = $postParams[$field];
            }
        }
        if (!filter_var($postParams['email'], FILTER_VALIDATE_EMAIL)) {
            $result['errors'][1] = 'Neįvestas arba blogai įvestas el. paštas';
            $result['success'] = false;
        }
        return $result;
    }
    
}
