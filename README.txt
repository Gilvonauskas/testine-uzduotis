Pasileisdami aplikaciją savo serveryje, turite nustatyti šiuos dalykus:

1. Faile "/config/config.php" nustatyti konstantą BASE_URL į jūsų šakninį aplikacijos katalogą, pvz., http://domenas.com/eshop , arba nurodyti savo virtual host'o URL, pvz.: http://subdomenas.domenas.com;
2. Direktorijai "/data" turite suteikti įrašymo teises "777" arba tokias siauresnes teises, kurios leistų aplikacijai rašyti į failus "/data" direktorijoje;
3. Jei jūsų serveris ne Apache, praleiskite 4 ir 5 punktus ir savo nuožiūra nustatykite, kad URL dalys "/kontroleris/actionas" būtų verčiamos į GET parametrus;
4. Turite padaryti, kad šakniniame aplikacijos kataloge arba virtual host'e būtų leidžiama įjungti Apache RewriteEngine nustatymą;
5. Turite padaryti, kad aplikacijos šakniniame kataloge esančio .htaccess failo 7 eilutė pavertinėtų URL dalis "/kontroleris/actionas" į GET parametrus.

-- DUOMENŲ GENERAVIMAS (SEED) --
Duomenų generavimo skriptas guli šakninės aplikacijos direktorijos seed.php faile. Prieš jį iškviesdami, turite nutrinti "die();" funkciją skripto pradžioje.

-- AUTORYSTĖ IR UŽDUOTIES REIKALAVIMŲ ATITIKIMAS --
Autorius: Tomas Gilvonauskas. Kaip buvo parašyta užduotyje, sukurta MVC pagrindu maža e-parduotuvė, naudojant tik "gryną" PHP, be jokių framework'ų. Klasių load'inimas ir route'inimas į controller'ių metodus padarytas remiantis šiuo video tutorialų ciklu: https://www.youtube.com/watch?v=Aw28-krO7ZM , tik pirma ir antra dalimi.
