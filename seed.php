<?php

//die(':]');

require_once 'config/config.php';
require_once 'models/Products.php';
require_once 'models/Orders.php';

$products = Products::seed();
echo 'Products file seeding has been executed'.'<br/>';

$orderedProducts = array(
    '1'=>array('quantity'=>1),
    '2'=>array('quantity'=>1)
);
file_put_contents('data/orders.txt', '');
// jungtis su: "Petras Petrauskas petras@example.com"
Orders::putNewOrder($orderedProducts, $products, 'PetrasPetrauskaspetras@example.com');
echo 'Orders file seeding has been executed'.'<br/>';
