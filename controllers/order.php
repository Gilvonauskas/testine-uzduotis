<?php

/*
 * License: free to use.
 */

/**
 * 
 *
 * @author Tomas Gilvonauskas
 */
class order extends Controller {
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        if (isset($_SESSION['errors'])) {
            $this->view->errors = $_SESSION['errors'];
            unset($_SESSION['errors']);
        }
        if (isset($_SESSION['field_values'])) {
            $this->view->field_values = $_SESSION['field_values'];
            unset($_SESSION['field_values']);
        }
        
        if (isset($_SESSION['user']['id']) && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->makeOrderAndRedirectToOrdersList();
        }
        elseif (isset($_SESSION['user']['id'])) {
            header('Location: ' . BASE_URL . '/order/orderslist' );
            die();
        }
        elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $validationResult = LoginForm::validate($_POST);
            if (true === $validationResult['success']) {
                $_SESSION['user']['id'] = $_POST['name'].$_POST['surname'].$_POST['email'];
                $this->makeOrderAndRedirectToOrdersList();
            }
            else {
                $_SESSION['errors'] = $validationResult['errors'];
                $_SESSION['field_values'] = $validationResult['field_values'];
                header('Location: ' . BASE_URL . '/order' );
                die();
            }
        }
        $this->view->render('order/index');
    }
    
    public function orderslist()
    {
        if (!isset($_SESSION['user']['id'])) {
            header('Location: ' . BASE_URL . '/order' );
            die();
        }
        
        if (isset($_SESSION['messages'])) {
            $this->view->messages = $_SESSION['messages'];
            unset($_SESSION['messages']);
        }
        
        $this->view->orders = Orders::getAllOrdersByUserId($_SESSION['user']['id']);
        $this->view->render('order/orderslist');
    }
    
    private function makeOrderAndRedirectToOrdersList()
    {
        $itemsInCart = CartCookie::getCartItems();
        if (count($itemsInCart)>0) {
            $productsInfo = Products::getProductsByIds(array_keys($itemsInCart));
            Orders::putNewOrder($itemsInCart, $productsInfo, $_SESSION['user']['id']);
            Products::updateProductsAmountsAfterOrder($itemsInCart);
        }
        CartCookie::unsetCartCookie();
        $_SESSION['messages'][] = 'Jūsų užsakymas priimtas';
        header('Location: ' . BASE_URL . '/order/orderslist');
        die();
    }
    
    public function vieworder()
    {
        if (!isset($_SESSION['user']['id']) || !isset($_GET['id']) || !is_numeric($_GET['id'])) {
            header('Location: ' . BASE_URL . '/order' );
            die();
        }
        $this->view->order_id = $_GET['id'];
        $this->view->order = Orders::getIndividualOrder($_SESSION['user']['id'], $_GET['id']);
        $this->view->render('order/vieworder');
    }
    
    public function cancelorder()
    {
        if(!isset($_POST['id'])) {
            header('Location: ' . BASE_URL . '/order' );
            die();
        }
        $order = Orders::cancelOrder($_SESSION['user']['id'], $_POST['id']);
        Products::updateAmountsAfterCanceledOrder($order);
        $_SESSION['messages'][] = 'Užsakymas, kurio ID '.$_POST['id'].', atlikimo data '.$order['date'].', atšauktas.';
        header('Location: ' . BASE_URL . '/order' );
        die();
    }
    
}
