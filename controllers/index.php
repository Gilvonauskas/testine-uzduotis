<?php

/*
 * License: free to use.
 */

/**
 * 
 *
 * @author Tomas Gilvonauskas
 */
class index extends Controller {
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        $this->view->products = Products::getAllProducts();
        $this->view->render('index/index');
    }
    
}
