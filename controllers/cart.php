<?php

/*
 * License: free to use.
 */

/**
 * 
 *
 * @author Tomas Gilvonauskas
 */
class cart extends Controller {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index()
    {
        if (isset($_COOKIE['cart_items_cookie'])) {
            $itemsInCart = json_decode($_COOKIE['cart_items_cookie'], true);
            $products = Products::getProductsByIds(array_keys($itemsInCart));
            $this->view->products = $products;
            $this->view->quantities = $itemsInCart;
            $this->view->total = Products::getProductsTotalPrice($products, $itemsInCart);
            $this->view->user_logged_in = isset($_SESSION['user']['id']);
        }
        else {
            $this->view->products = array();
        }
        $this->view->render('cart/index');
    }
    
    public function addtocart()
    {
        $id = $_GET['product_id'];
        if (isset($_GET['quantity']) && is_numeric($_GET['quantity'])) {
            $quantity = $_GET['quantity'];
        }
        else {
            $quantity = 1;
        }
        if (isset($id) && is_numeric($id)) {
            if (Products::getProduct($id) != false) {
                $itemsInCart = CartCookie::getCartItems();
                if (!is_null($itemsInCart) && array_key_exists($id, $itemsInCart)) {
                    $itemsInCart[$id]['quantity'] = $itemsInCart[$id]['quantity'] + $quantity;
                }
                else {
                    $itemsInCart[$id]['quantity'] = $quantity;
                }
                CartCookie::setCartItems($itemsInCart);
            }
        }
        die();
    }
    
    public function changequantity()
    {
        if (isset($_GET['product_id']) && is_numeric($_GET['product_id']) && isset($_GET['quantity']) && is_numeric($_GET['quantity'])) {
            $id = $_GET['product_id'];
            if (Products::getProduct($id) != false) {
                $itemsInCart = CartCookie::getCartItems();
                if ($_GET['quantity']>0) {
                    $itemsInCart[$id]['quantity'] = $_GET['quantity'];
                }
                else {
                    unset($itemsInCart[$id]);
                }
                CartCookie::setCartItems($itemsInCart);
            }
        }
        die();
    }
    
}
